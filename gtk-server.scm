(define-record gtk-server in out)

(define required-args '("-stdin"))

(define (start-gtk-server! #!key (program-name "gtk-server") (args '()))
  (receive (in out _pid) (process program-name (append required-args args))
    (make-gtk-server in out)))

(define (gtk-send! gtk-server #!rest args)
  (write-line (string-intersperse (map ->string args) " ")
              (gtk-server-out gtk-server))
  (read-line (gtk-server-in gtk-server)))

(define (stop-gtk-server! gtk-server)
  (when (and (gtk-server-in gtk-server) (gtk-server-out gtk-server))
    (close-input-port (gtk-server-in gtk-server))
    (close-output-port (gtk-server-out gtk-server))
    (gtk-server-in-set! gtk-server #f)
    (gtk-server-out-set! gtk-server #f)))
