## About

A CHICKEN interface to [GTK-server]. All examples have been tested
with the GTK3 version and use stdin mode.

## Docs

See [its wiki page].

[GTK-server]: http://gtk-server.org/
[its wiki page]: http://wiki.call-cc.org/eggref/5/gtk-server
