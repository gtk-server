(import scheme)
(import (chicken base))
(import (chicken pathname))
(import (chicken process-context))
(import gtk-server)

(define gtk-server (start-gtk-server!))
(define (gtk #!rest args) (apply gtk-send! gtk-server args))

(gtk "gtk_init" 'NULL 'NULL)
(gtk "gtk_server_define gtk_builder_new_from_file NONE WIDGET 1 STRING")
(gtk "gtk_server_define gtk_builder_get_object NONE WIDGET 2 WIDGET STRING")
(define widget #f)

;; Get Glade file
(define ui-file (pathname-replace-extension (program-name) ".ui"))
(define xml (gtk "gtk_builder_new_from_file" ui-file))

;; Get main window ID and connect signal
(set! widget (gtk "gtk_builder_get_object" xml "window"))
(gtk "gtk_server_connect" widget "delete-event" "window")

;; Get exit button ID and connect signal
(set! widget (gtk "gtk_builder_get_object" xml "exit_button"))
(gtk "gtk_server_connect" widget "clicked" "exit_button")

;; Get print button ID and connect signal
(set! widget (gtk "gtk_builder_get_object" xml "print_button"))
(gtk "gtk_server_connect" widget "clicked" "print_button")

;; Get entry ID
(define entry (gtk "gtk_builder_get_object" xml "entry"))

;; Mainloop
(let loop ()
  (let ((event (gtk "gtk_server_callback wait")))
    (when (not (member event '("window" "exit_button")))
      (when (equal? event "print_button")
        (print (gtk "gtk_entry_get_text" entry)))
      (loop))))

;; Exit GTK
(gtk "gtk_server_exit")
(stop-gtk-server! gtk-server)
