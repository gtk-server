(import scheme)
(import (chicken base))
(import gtk-server)

(define gtk-server (start-gtk-server! args: '("-log=/tmp/gtk-server.log")))
(define (gtk #!rest args) (apply gtk-send! gtk-server args))

;; Define GUI - mainwindow
(define win (gtk "m_window" "'Testing testing 123'" 500 500))
(gtk "m_bgcolor" win "#aaE8aa")
;; Define multiline textfield
(define txt (gtk "m_text" 480 200))
(gtk "m_attach" win txt 10 10)
(gtk "m_basecolor" txt "#AAAA00")
(gtk "m_textcolor" txt "#FFFF33")
(gtk "m_text_text" txt "'Start typing here...'")
(gtk "m_font" txt "'Monospace 12'")
;; Define a multiline list
(define lst (gtk "m_list" 480 220))
(gtk "m_attach" win lst 10 220)
(gtk "m_list_text" lst "Hello")
(gtk "m_list_text" lst "\"'This is a list box'\"")
(gtk "m_list_text" lst "\"'1 2 3'\"")
(gtk "m_list_text" lst "\"'We can add many items here'\"")
(gtk "m_list_text" lst "\"'...and even more'\"")
(gtk "m_font" lst "'Serif 10'")
(gtk "m_basecolor" lst "#5555FF")
(gtk "m_fgcolor" lst "#FFFF22")
;; Create buttons
(define get (gtk "m_stock" 'gtk-info 100 40))
(gtk "m_attach" win get 10 450)
(define exit (gtk "m_stock" 'gtk-quit 100 40))
(gtk "m_attach" win exit 390 450)
;; Create combobox
(define combo (gtk "m_combo" "'bla 123'" 200 30))
(gtk "m_combo_text" combo "'more text'")
(gtk "m_bgcolor" combo "#00FF00" "#00FF00" "#00FF00" "#00FF00" "#00FF00")
(gtk "m_textcolor" combo "#FF00FF" "#FF00FF" "#FF00FF" "#FF00FF" "#FF00FF")
(gtk "m_attach" win combo 130 450)
(gtk "m_combo_set" combo 0)
;; Focus to textwidget
(gtk "m_focus" txt)

;; Mainloop
(let loop ()
  (let ((event (gtk "m_event")))
    (when (not (member event (list exit win)))
      (cond
       ((equal? event get)
        (print (gtk "m_list_grab" lst)))
       ((equal? event combo)
        (print (gtk "m_combo_grab" combo))))
      (loop))))

;; Exit GTK
(gtk "m_end")
(stop-gtk-server! gtk-server)
