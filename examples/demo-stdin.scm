(import scheme)
(import (chicken base))
(import (chicken process signal))
(import gtk-server)

(set-signal-handler! signal/chld (lambda x (exit)))

(define gtk-server (start-gtk-server!))
(define (gtk #!rest args) (apply gtk-send! gtk-server args))

(gtk "gtk_init" 'NULL 'NULL)
(define window (gtk "gtk_window_new" 0))
(gtk "gtk_window_set_title" window "'This is a title'")
(gtk "gtk_window_set_position" window 1)
(define table (gtk "gtk_table_new" 10 10 1))
(gtk "gtk_container_add" window table)
(define button (gtk "gtk_button_new_with_label" "'Click here to exit!'"))
(gtk "gtk_table_attach_defaults" table button 5 9 7 9)
(define check (gtk "gtk_check_button_new_with_label" "'Check \t this \n out!'"))
(gtk "gtk_table_attach_defaults" table check 1 6 1 2)
(define entry (gtk "gtk_entry_new"))
(gtk "gtk_table_attach_defaults" table entry 1 6 3 4)
(gtk "gtk_window_set_type_hint" window 'GDK_WINDOW_TYPE_HINT_UTILITY)
(gtk "gtk_widget_show_all" window)

;; (gtk "gtk_server_connect" window "key-press-event" "keypressed")

(let loop ()
  (let ((event (gtk "gtk_server_callback wait")))
    (when (not (equal? event button))
      (when (equal? event entry)
        (print (gtk "gtk_entry_get_text" entry)))
      (when (equal? event "keypressed")
        (print "Key pressed: " (gtk "gtk_server_key"))
        (print "Key state: " (gtk "gtk_server_state")))
      (loop))))

;; Exit GTK
(stop-gtk-server! gtk-server)
