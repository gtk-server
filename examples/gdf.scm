(import scheme)
(import (chicken base))
(import (chicken format))
(import (chicken io))
(import (chicken process))
(import (chicken string))
(import (srfi 1))
(import gtk-server)

(define gtk-server (start-gtk-server!))
(define (gtk #!rest args) (apply gtk-send! gtk-server args))

(define (capture command-string)
  (call-with-input-pipe command-string
    (lambda (in)
      (string-chomp (read-string #f in)))))

(define info
  (map string-split
       (string-split (capture "df | awk '/\\// {print $2,$3,$6}'") "\n")))

(define sizes (list->vector (map (o string->number first) info)))
(define maxsize (apply max (vector->list sizes)))
(define used (list->vector (map (o string->number second) info)))
(define names (list->vector (map third info)))

(define marge 10)
(define xpos 20)
(define ypos 0)
(define width1 90)
(define width2 80)

(define screenw (+ (* width1 (vector-length sizes))
                   (* marge (vector-length sizes))
                   50))
(define framew (- screenw 10))
(define canvasw (- screenw 30))
(define linew (- screenw 50))

;; Define GUI - mainwindow
(define win (gtk "m_window" "\"'Graphical Disk Fill'\"" screenw 400))
(gtk "m_bgcolor" win "#FFFFFF")
;; Attach frame
(define frame (gtk "m_frame" framew 390))
(gtk "m_bgcolor" frame "#FFFFFF")
(gtk "m_frame_text" frame "\"' Disk Usage Graph '\"")
(gtk "m_attach" win frame 5 5)
;; Setup the drawing canvas, draw stuff
(define canvas (gtk "m_canvas" canvasw 365))
(gtk "m_attach" win canvas 10 25)
;; Draw axis
(gtk "m_line" "#000000" 20 340 linew 340)

(let loop ((idx 0))
  (when (< idx (vector-length sizes))
    ;; Draw columns
    (let ((height (inexact->exact
                   (floor
                    (* (/ (vector-ref sizes idx) maxsize) 310)))))
      (set! ypos (- 340 height))
      (gtk "m_square" "#00FF11" xpos ypos width1 height 1)
      (gtk "m_square" "#000000" xpos ypos width1 height 0))
    ;; Draw amount
    (set! ypos (- ypos 15))
    (gtk "m_out" (vector-ref sizes idx) "#007700" "rgba(0,0,0,0)" xpos ypos)
    ;; Draw usage
    (let ((height (inexact->exact
                   (floor
                    (* (/ (vector-ref used idx) maxsize) 310)))))
      (set! xpos (+ xpos 5))
      (set! ypos (- 340 height))
      (gtk "m_square" "#FF1100" xpos ypos width2 height 1)
      (gtk "m_square" "#000000" xpos ypos width2 height 0))
    ;; Draw text
    (set! ypos 345)
    (gtk (format "m_out \"'~a'\" #0000FF rgba(0,0,0,0) ~a ~a" (vector-ref names idx) xpos ypos))
    (set! xpos (+ xpos width1 marge -5))
    ;; Next column
    (loop (+ idx 1))))

;; Mainloop
(let loop ()
  (let ((event (gtk "m_event")))
    (when (not (equal? event win))
      (loop))))

;; Exit GTK
(gtk "m_end")
(stop-gtk-server! gtk-server)
