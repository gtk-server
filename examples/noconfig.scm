(import scheme)
(import (chicken base))
(import gtk-server)

(define gtk-server (start-gtk-server!))
(define (gtk #!rest args) (apply gtk-send! gtk-server args))

;; Check and open GTK library first
(unless (equal? (gtk "gtk_server_require" "libgtk-3.so") "ok")
  (print "GTK 3.x not found on your system!")
  (stop-gtk-server! gtk-server)
  (exit 1))

;; Define the GTK functions we need
(gtk "gtk_server_define gtk_init NONE NONE 2 NULL NULL")
(gtk "gtk_server_define gtk_window_new delete-event WIDGET 1 LONG")
(gtk "gtk_server_define gtk_window_set_title NONE NONE 2 WIDGET STRING")
(gtk "gtk_server_define gtk_window_set_position NONE NONE 2 WIDGET LONG")
(gtk "gtk_server_define gtk_table_new NONE WIDGET 3 LONG LONG BOOL")
(gtk "gtk_server_define gtk_container_add NONE NONE 2 WIDGET WIDGET")
(gtk "gtk_server_define gtk_button_new_with_label clicked WIDGET 1 STRING")
(gtk "gtk_server_define gtk_table_attach_defaults NONE NONE 6 WIDGET WIDGET LONG LONG LONG LONG")
(gtk "gtk_server_define gtk_check_button_new_with_label clicked WIDGET 1 STRING")
(gtk "gtk_server_define gtk_entry_new activate WIDGET 0")
(gtk "gtk_server_define gtk_widget_show_all NONE NONE 1 WIDGET")
(gtk "gtk_server_define gtk_entry_get_text NONE STRING 1 WIDGET")

(gtk "gtk_init" 'NULL 'NULL)
(define window (gtk "gtk_window_new" 0))
(gtk "gtk_window_set_title" window "'This is a title'")
(gtk "gtk_window_set_position" window 1)
(define table (gtk "gtk_table_new" 10 10 1))
(gtk "gtk_container_add" window table)
(define button (gtk "gtk_button_new_with_label" "'Click here to exit!'"))
(gtk "gtk_table_attach_defaults" table button 5 9 7 9)
(define check (gtk "gtk_check_button_new_with_label" "'Check \t this \n out!'"))
(gtk "gtk_table_attach_defaults" table check 1 6 1 2)
(define entry (gtk "gtk_entry_new"))
(gtk "gtk_table_attach_defaults" table entry 1 6 3 4)
(gtk "gtk_widget_show_all" window)

;; Mainloop
(let loop ()
  (let ((event (gtk "gtk_server_callback wait")))
    (when (not (equal? event button))
      (when (equal? event entry)
        (print (gtk "gtk_entry_get_text" entry)))
      (loop))))

;; Exit GTK
(gtk "gtk_server_exit")
(stop-gtk-server! gtk-server)
