(module gtk-server (start-gtk-server! gtk-send! stop-gtk-server!)
  (import scheme)
  (import (chicken base))
  (import (chicken io))
  (import (chicken process))
  (import (chicken string))

  (include "gtk-server.scm")

)
